module Helpers exposing (..)

import Model exposing (..)
import Dict exposing (get, insert, empty, merge)
import Set exposing (Set, member, insert, empty, remove, toList)
import MyTable exposing (initialSort)
import Extras exposing (message)
import JsonHelpers exposing (..)
import Navigation exposing (newUrl, modifyUrl, Location)
--------------------------------------------------------------------------------
validateBucketName : String -> Result String String
validateBucketName pathPrefix =
    let 
        str = String.trim pathPrefix |>
                chkEmpty  |> Result.andThen
                chkEnding 

        chkEmpty: String -> Result String String            
        chkEmpty str = if not (String.isEmpty str) then
                            Ok str
                        else 
                            Err "You must enter a valid bucketname"
        
        chkEnding: String -> Result String String
        chkEnding str = if String.endsWith "/" str then
                            Ok str
                        else
                            Err "The Bucket path must be the bucket name or a valid directory path followed by '/' "
    in
        str
--------------------------------------------------------------------------------

splitBktPath : String -> ( String, String )
splitBktPath pathStr = 
    let 
        bktName = String.split "/" pathStr  |> List.head |> Maybe.withDefault ""
        prefix  = String.split "/" pathStr |> List.tail 
                                                |> Maybe.map ( String.join "/"  )
                                                |> Maybe.withDefault ""
        
    in
        (bktName, prefix)        

--------------------------------------------------------------------------------

getListObject: Model -> (Model, Cmd Msg)
getListObject model = 
    let    
        (bktName, prefix ) = splitBktPath model.pathPrefix
        newUrlString = model.loc.origin ++ model.loc.pathname ++ "#" ++ model.pathPrefix

        getBktLoc = 
            if Dict.member bktName model.bktLocation then
                Cmd.none
            else
                (getBucketLocation bktName)
        
        (newModel, cmd) = 
            if model.loc.hash == ("#" ++ model.pathPrefix) then
                ( {model | ignoreUrlChange = False }, 
                    Cmd.batch [ (sendListObjectV2 bktName prefix model)
                                , getBktLoc ]
                )
            else
                ( { model | ignoreUrlChange = True }, 
                    Cmd.batch [ (sendListObjectV2 bktName prefix model) 
                                , newUrl newUrlString 
                                , getBktLoc ]
                )
    in            
        if bktName == "" then
            ( { model | logs = model.logs ++ [("ERROR", ["Bucket name could not be deciphered"])] }
            , Cmd.none)
        else
             (newModel, cmd )
                    
--------------------------------------------------------------------------------

getMoreListObject: Model -> (Model, Cmd Msg)
getMoreListObject model = 
    let    
        (bktName, prefix ) = splitBktPath model.pathPrefix
        dir = Dict.get model.pathPrefix model.bkt
    in            
        case dir of 
            Just dirX ->
                ( model, sendMoreListObjectV2 bktName prefix dirX.continuationToken)
            Nothing ->
                ( { model | logs = model.logs ++ [("ERROR", ["Fatal error.. Please reload the page"])] }
                , Cmd.none)
                

--------------------------------------------------------------------------------

toggle : String -> Set String -> Set String
toggle key selectedKeys =
  if Set.member key selectedKeys then
    Set.remove key selectedKeys
  else
    Set.insert key selectedKeys
--------------------------------------------------------------------------------

bktLocation : String -> Model -> (Model, Cmd Msg)
bktLocation jsonString model =
    let 
        loc = locDecoder jsonString
        (bktLocation, errors) = 
            case loc of
                Ok x ->
                    (Dict.insert x.bucket ("https://s3-" ++ x.region ++ ".amazonaws.com/") model.bktLocation
                    , model.logs )
                Err err ->
                    (model.bktLocation, model.logs ++ [("Error", [err])])
    in
        ( { model | bktLocation = bktLocation
                  , logs = errors}, Cmd.none)

--------------------------------------------------------------------------------

updateBucket : String -> Model -> (Model, Result String Dir)
updateBucket jsonString model =
    let 
        dir = Result.map customEditDir (bktDecoder jsonString)
        (bkt, errors) = 
            case dir of
                Ok dirx ->
                    ( (updateDataStore dirx model.bkt ), model.logs )
                Err err ->
                    (model.bkt, model.logs ++ [("Error", [err])])
    in
        ( { model | bkt = bkt
                  , logs = errors}, dir)

--------------------------------------------------------------------------------
customEditDir : Dir -> Dir
customEditDir dir = 
    let 
        newPrefix = dir.name ++ "/" ++ dir.prefix
        newContents = 
            List.filterMap (\ elem ->
                                if dir.prefix /= elem.keyName then
                                    Just elem
                                else
                                    Nothing
                            ) dir.contents
    in
        { dir | prefix = newPrefix, contents = newContents }

--------------------------------------------------------------------------------

updateDataStore: Dir -> Bkt -> Bkt
updateDataStore dir bkt = 
    let 
        newBkt = Dict.insert dir.prefix dir Dict.empty
    in
        Dict.merge Dict.insert mergeBkts Dict.insert newBkt bkt Dict.empty 

--------------------------------------------------------------------------------
-- mergeBkts : String -> Dir -> Dir -> Bkt
mergeBkts key newValues oldValues dict = 
    let 
        name = newValues.name
        prefix = newValues.prefix

        isTruncated = newValues.isTruncated && oldValues.isTruncated
        commonPrefix =  newValues.commonPrefix ++ oldValues.commonPrefix

        contents = newValues.contents ++ oldValues.contents
        continuationToken = newValues.continuationToken
        mergedValues = Dir name prefix isTruncated commonPrefix contents continuationToken
    in    
        Dict.insert key mergedValues dict

------------------------------------------------------------------

updateTable : String -> Model -> (Model, Cmd Msg)
updateTable dirName model = 
    if dirName == model.pathPrefix then
        (  model , Cmd.none )
    else
        ( { model | selectedKeys = Set.empty, selectall = True}, Cmd.none )
        
--------------------------------------------------------------------------------

downloadElement : Model -> Cmd Msg
downloadElement model =
    let 
        cmdList = List.map ( \ str -> 
                            let 
                                (bkt, key) = splitBktPath str
                                cmd = 
                                    case Dict.get bkt model.bktLocation of
                                        Just v ->
                                            downloadObject (v ++ str)
                                        Nothing -> 
                                            getObject bkt key
                            in
                                cmd
                        ) <| Set.toList model.selectedKeys
    in
        Cmd.batch cmdList

--------------------------------------------------------------------------------

downloadTorrent model = 
    let 
        cmdList = List.map ( \ str -> 
                            let 
                                (bkt, key) = splitBktPath str
                            in
                                getObjectTorrent bkt key
                        ) <| Set.toList model.selectedKeys
    in
        Cmd.batch cmdList

--------------------------------------------------------------------------------
-- CACHE stuff

fromState : CacheModel -> Location -> (Model, Cmd Msg)
fromState jsmodel location =
    let 
        cmd = 
            if location.hash == "" then
                let 
                    newUrlString = location.origin ++ location.pathname ++ jsmodel.hash
                in
                    modifyUrl newUrlString
            else
                Cmd.none
            
        model = Model
                    jsmodel.pathPrefix
                    (Dict.fromList jsmodel.bkt)
                    (Dict.fromList jsmodel.bktLocation)
                    jsmodel.query
                    jsmodel.selectall
                    (Set.fromList jsmodel.selectedKeys)
                    (MyTable.initialSort "")
                    jsmodel.ignoreUrlChange
                    location
                    []
    in 
        (model, cmd)


setState : Model -> CacheModel
setState model = 
    CacheModel
        model.pathPrefix
        (Dict.toList model.bkt)
        (Dict.toList model.bktLocation)
        model.query
        model.selectall
        (Set.toList model.selectedKeys)
        model.ignoreUrlChange
        model.logs
        model.loc.hash


--------------------------------------------------------------------------------
-- DISPLAY table 

displayTable : String -> Cmd Msg    
displayTable dirName =
    Extras.message (DisplayTable dirName)
        
setTableState : Cmd Msg
setTableState = 
    Extras.message (SetTableState (MyTable.initialSort ""))
        

fetchDir : Cmd Msg
fetchDir = 
    Extras.message (FetchBucket)
    
openDir : String  -> Cmd Msg
openDir pathPrefix = 
    Extras.message (OpenDir pathPrefix)