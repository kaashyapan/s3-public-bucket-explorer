module JsonHelpers exposing (..)

import Model exposing (..)
import Ports exposing (..)
import Json.Decode exposing (..)
import Json.Encode exposing (..)


getObject bktName keyName = 
    let 
        getObjectReq = object [  ("Bucket", Json.Encode.string bktName),
                                 ("Key", Json.Encode.string keyName)]
    in
        toGetObject (encode 0 getObjectReq)

downloadObject uri = 
    let 
        downObjectReq = object [  ("uri", Json.Encode.string uri)]
    in
        toDownObject (encode 0 downObjectReq)

getBucketLocation bktName = 
    let 
        getBktLocationReq = object [  ("Bucket", Json.Encode.string bktName)]
                         
    in
        toGetBucketLocation (encode 0 getBktLocationReq)

getObjectTorrent bktName keyName = 
    let 
        getObjectTorrentReq = object [  ("Bucket", Json.Encode.string bktName),
                                        ("Key", Json.Encode.string keyName)]
    in
        toGetObjectTorrent (encode 0 getObjectTorrentReq)

sendListObjectV2 bktName prefix model = 
    let 
        listObjectv2Req = object [  ("Bucket", Json.Encode.string bktName),
                                    ("Delimiter", Json.Encode.string "/"),
                                    ("Prefix", Json.Encode.string prefix)]
    in
        toListObjectsv2 (encode 0 listObjectv2Req)
    
sendMoreListObjectV2 bktName prefix continuationToken = 
    let 
        listObjectv2Req = 
            case continuationToken of 
                Just token ->
                    object [  ("Bucket", Json.Encode.string bktName),
                              ("Delimiter", Json.Encode.string "/"),
                              ("Prefix", Json.Encode.string prefix),
                              ("ContinuationToken", Json.Encode.string token)]
                Nothing ->
                    object [  ("Bucket", Json.Encode.string bktName),
                              ("Delimiter", Json.Encode.string "/"),
                              ("Prefix", Json.Encode.string prefix)]

    in
        toListObjectsv2 (encode 0 listObjectv2Req)


bktDecoder : String -> Result String Dir
bktDecoder jsonString = 
        Json.Decode.decodeString dirDecoder jsonString

dirDecoder: Decoder Dir
dirDecoder = 
         Json.Decode.map6 Dir
          (Json.Decode.field "Name" Json.Decode.string)
          (Json.Decode.field "Prefix" Json.Decode.string)
          (Json.Decode.field "IsTruncated" Json.Decode.bool)
          (Json.Decode.field "CommonPrefixes" (Json.Decode.list commonPrefixDecoder))
          (Json.Decode.field "Contents" (Json.Decode.list elementDecoder))
          continuationTokenDecoder

continuationTokenDecoder = 
    Json.Decode.maybe (Json.Decode.field "NextContinuationToken" Json.Decode.string) 


commonPrefixDecoder: Decoder String
commonPrefixDecoder = 
        Json.Decode.field "Prefix" Json.Decode.string

elementDecoder: Decoder BktElement
elementDecoder = 
        Json.Decode.map5 BktElement
        (Json.Decode.field "Key" Json.Decode.string)
        (Json.Decode.field "LastModified" Json.Decode.string)        
        (Json.Decode.field "ETag" Json.Decode.string)                
        (Json.Decode.field "Size" Json.Decode.float)
        (Json.Decode.field "StorageClass" Json.Decode.string)                

locDecoder : String -> Result String Loc
locDecoder jsonString = 
        Json.Decode.decodeString locationDecoder jsonString


locationDecoder: Decoder Loc
locationDecoder = 
        Json.Decode.map2 Loc
        (Json.Decode.field "Bucket" Json.Decode.string)
        (Json.Decode.field "LocationConstraint" Json.Decode.string)