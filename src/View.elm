module View exposing (..)

import MyTable exposing (..)
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes as Attr exposing (..)
import Model exposing (..)
import Dict exposing (get)
import Tuple exposing (first, second)
import Set exposing (size, Set, member)
import Helpers exposing (splitBktPath)
import String exposing (toLower, contains, length, slice, endsWith, dropLeft, startsWith, indexes)
import List exposing (reverse, take, maximum)

view : Model -> Html Msg
view model =
    body [] [ mainView model 
            , div[] [bucketView model]
            , div[logStyle] [logView model]
            ]

        
mainStyle = 
    style [  ("position", "absolute")
            ,("width", "60%")
            ,("top", "10px")
            ,("left", "10px")
        ]

mainView model = 
 
     div[]   [ div [bgImage][]
            ,  div [mainStyle] 
                 [span [myButStyle] [img [src "www/images/favicon.png", height 18, width 18  ][] ]
                 ,span [myButStyle] [bucketName model]
                 ,button [myButStyle
                        , Html.Events.onClick FetchBucket
                        , class "btn btn-primary"] 
                        [ text "Sync S3"]
                ]
            ]

tabStyle = 
    style [  ("position", "absolute")
            ,("height", "100%")
            ,("width", "100%")
            ,("overflow-x", "auto")
            ,("overflow-y", "auto")
            ,("top", "60px")
            ,("left", "10px")
        ]

bucketStyle = 
    style [  ("position", "absolute")
            ,("height", "80%")
            ,("width", "65%")
            ,("top", "60px")
            ,("left", "10px")
        ]


bucketView model = 
    let 
        dirName = model.pathPrefix
        
        (displayTable, isTruncated, tableElements) =
            case get dirName model.bkt of
                Just dir ->
                    (True, dir.isTruncated, datatransform dir model.selectedKeys)
                Nothing ->
                    (False, False, [tableElementInit])
    in
        if displayTable then
            let
                lowerQuery = toLower model.query
                acceptableElements =
                    List.filter (contains lowerQuery << toLower << .keyName) tableElements
            in
                div [bucketStyle] 
                        [input [class "input-sm", placeholder "Search by Name", onInput SetQuery ] []
                        ,div [class "btn-group float-right"] 
                            [button [Html.Events.onClick Download , class "btn btn-sm"]
                                    [span [class "icon ion-ios-cloud-download-outline"][], text "Download"]
                            ,button [Html.Events.onClick DownloadTorrent, class "btn btn-sm"]
                                    [span [class "icon ion-magnet"][], text "Torrent"]                               
                            ]
                        ,(  if isTruncated then
                                div [class "abs", style [("left","50%")] ]
                                    [div [Html.Events.onClick (FetchMore model.pathPrefix ), 
                                            class "btn btn-link btn-sm"
                                         ]
                                         [text "This list is incomplete...Fetch more ?"] 
                                    ]
                            else
                                div [] []
                        )
                        ,div [tabStyle] [MyTable.view ( config model acceptableElements) model.tableState acceptableElements]
                        ]
        else
            div [][]

datatransform : Dir -> Set String -> List TableElement    
datatransform dir selectedKey = 
        (transformContents dir selectedKey )
    ++  (transformPrefix dir selectedKey )

transformContents : Dir -> Set String -> List TableElement    
transformContents dir selectedKey = 
    List.map (\ elem -> 
        let 
            keyName =  formatKeyName elem.keyName dir.prefix
            lastModified = elem.lastModified
            eTag = elem.eTag
            size = elem.size
            storageClass = elem.storageClass
        in
            TableElement dir.name dir.prefix keyName lastModified eTag size storageClass 
                        (member (dir.prefix ++ keyName) selectedKey)
    ) dir.contents

formatKeyName : String -> String -> String
formatKeyName keyName fullPrefix = 
    let 
        ( _ , prefix) = splitBktPath fullPrefix
    in
        if startsWith prefix keyName then
            dropLeft (length prefix) keyName
        else
            keyName


transformPrefix : Dir -> Set String -> List TableElement    
transformPrefix dir selectedKey = 
    List.map (\ dirName -> 
        let 
            keyName =  formatKeyName dirName dir.prefix
        in
            TableElement dir.name dir.prefix keyName "" "" 0 ""
                        (member (dir.prefix ++ keyName)  selectedKey)
    ) dir.commonPrefix


bucketName model = 
    input [ placeholder "my-bucketname/directory/", 
            autofocus True,
            onInput NewPath,
            myInputStyle,
            value model.pathPrefix
            ] 
          []            
 
myButStyle = 
     style [  ("position", "relative")
            , ("padding", "5px 5px")
--            , ("color", "white")
            ]

myInputStyle =
  style
    [ ("width", "40%")
    , ("height", "5%")
    , ("position", "relative")
    , ("padding", "5px 0px")
    , ("text-align", "left")
    ]


bgImage = 
   style [  ("position", "fixed")
            ,("background-image", "url('www/images/background.png')")
            ,("background-position","center")
            ,("background-repeat", "no-repeat")
            ,("background-size", "40%")
            ,("opacity", "0.1")
            ,("z-index", "-1")
            ,("height", "100%")
            ,("width", "100%")
        ] 

logStyle = 
    style [ ("height", "50%")
            ,("width", "30%")
            ,("overflow-x", "auto")
            ,("overflow-y", "auto")
            ,("position", "fixed")
            ,("top", "10px")
            ,("right", "0")
        ]

logTextStyle = 
    style [ ("position", "relative")
            ,("margin-left", "5px")
            ,("font-family", "Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace")
        ]

logView model = 
    if List.length model.logs > 0 then
        div [] [  h4 [] [text "Messages  ", 
                         span   [Html.Events.onClick EmptyMsgs, style [("float","right"),("margin-right","7%")] ]
                                [i  [class "icon ion-trash-b", style [("font-size","18px")]] 
                                    []
                                ]
                        ]
                , div[] (List.map (\ str -> 
                                    case first str of 
                                        "ERROR" ->
                                            div [] ( [span [class "label label-danger"] [text "Err"]]
                                                      ++ ( List.map (\ logs -> span [logTextStyle] [text logs]
                                                                            ) (second str)
                                                                )
                                                    )
                                        "INFO" ->
                                            div [] ( [span [class "label label-primary"] [text "info"]]
                                                      ++ ( List.map (\ logs -> span [logTextStyle] [text logs]
                                                                  ) (second str)
                                                         )
                                                    )
                                        "SUCCESS" ->
                                            div [] ( [span [class "label label-success"] [text "success"]]
                                                      ++  ( List.map (\ logs -> span [logTextStyle] [text logs]
                                                                  ) (second str)
                                                         )
                                                    )
                                        
                                        _ ->
                                            div [] ( [span [class "label label-default"] [text "success"]]
                                                      ++  ( List.map (\ logs -> span [logTextStyle] [text logs]
                                                                  ) (second str)
                                                         )
                                                    )
                                                   
                                ) (List.reverse model.logs)
                    )
                ]
    else
        div [] []
            
-- TABLE CONFIGURATION


config : Model -> List TableElement -> MyTable.Config TableElement Msg
config model acceptableElements =
  MyTable.customConfig
    { toId = .keyName
    , toMsg = SetTableState
    , columns =
        [ checkboxColumn
        , iconColumn
        , keyNameColumn
        , lastModColumn
--        , MyTable.stringColumn "Etag" .eTag
        , sizeColumn
        , storageColumn
        ]
    , customizations =
        { defaultCustomizations | rowAttrs   = toRowAttrs
                                , tableAttrs = toTableAttrs
                                , thead      = customThead model acceptableElements }
                                
    }

toTableAttrs : List (Attribute msg)
toTableAttrs = 
    [style [("width","100%"),("table-layout","fixed")] ]

lastModColumn : MyTable.Column TableElement Msg
lastModColumn =
  MyTable.veryCustomColumn
    { name = "Last Modified"
    , viewData = lastModColumnView 
    , sorter = MyTable.increasingOrDecreasingBy .lastModified
    }

lastModColumnView : TableElement -> MyTable.HtmlDetails Msg
lastModColumnView tableElements = 
    MyTable.HtmlDetails [] [span [style [("overflow","hidden")]] [text tableElements.lastModified] ]


storageColumn : MyTable.Column TableElement Msg
storageColumn =
  MyTable.veryCustomColumn
    { name = "Class"
    , viewData = storageClassView 
    , sorter = MyTable.increasingOrDecreasingBy .storageClass
    }

storageClassView : TableElement -> MyTable.HtmlDetails Msg
storageClassView tableElements = 
    let 
        sClass = 
        case tableElements.storageClass of 
            "STANDARD" -> "STD"
            "STANDARD-IA" -> "STD-IA"
            "GLACIER" -> "GLACIER"
            "REDUCED_REDUNDANCY" -> "RRS"
            _ -> tableElements.storageClass
    in
        MyTable.HtmlDetails [] [span [style [("font-size","80%"),("overflow","hidden")]] [text sClass]]


keyNameColumn : MyTable.Column TableElement Msg
keyNameColumn = 
    MyTable.veryCustomColumn
        { name = "Name"
        , viewData = keyNameView
        , sorter = MyTable.increasingOrDecreasingBy .keyName
        }
    
keyNameView : TableElement -> MyTable.HtmlDetails Msg
keyNameView tableElements = 
    if ( endsWith "/" tableElements.keyName ) then
        MyTable.HtmlDetails [] [a [ href ("#" ++ tableElements.prefix ++ tableElements.keyName) ]
                                [text tableElements.keyName] ]
    else
        MyTable.HtmlDetails [] [div [style [("overflow","hidden")] ] [text tableElements.keyName] ]


iconColumn : MyTable.Column TableElement Msg
iconColumn = 
    MyTable.veryCustomColumn
        { name = ""
        , viewData = iconView << .keyName
        , sorter = MyTable.increasingOrDecreasingBy 
                    ( (\ s -> 
                        let 
                            (typ, html_) = keyType s
                        in
                            typ
                        ) << .keyName
                    )
        }

iconView: String -> MyTable.HtmlDetails Msg
iconView keyName = 
    let 
        (typ, html_) = keyType keyName
    in
        MyTable.HtmlDetails [] [html_]

keyType: String -> (String, Html Msg)
keyType keyName = 
    let 
        ext = 
            if endsWith "/" keyName then
                "./"
            else 
                indexes "." keyName |> maximum |> 
                Maybe.map (\ x -> slice x (length keyName) keyName) |>
                (\ x -> case x of 
                            Just x -> 
                                x
                            Nothing ->
                                "unknown"
                )
        isVideo = (\ x -> contains x ".mp4.avi.flv.divx.3gp")
        isAudio = (\ x -> contains x ".mp3.wav.flac.ogg.rm")
        isImage = (\ x -> contains x ".jpg.jpeg.png.tiff.ico.raw.bmp.gif")
        isFolder = (\ x -> contains x "./")
        isCode = (\ x -> contains x ".php.py.go.rb.css.html.js.c..cpp.jar.java.elm.json.xml.yaml.sh.rc.r.pl")
        isArchive = (\ x -> contains x ".zip.bz2.gzip.rar.gz")
        isTxt =  (\ x -> contains x ".txt.doc.rtf.docx.pdf")
        typHtml =
            if isFolder ext then
                span [class "icon ion-ios-folder-outline"] []
            else if isVideo ext then 
                span [class "icon ion-social-youtube"] []
            else if isImage ext then 
                span [class "icon ion-image"] []
            else if isCode ext then 
                span [class "icon ion-code"] []
            else if isArchive ext then
                span [class "icon ion-archive"] []
            else if isArchive ext then
                span [class "icon ion-document-text"] []
            else
                span [class "icon ion-document"] []
    in
        (ext, typHtml)

sizeColumn : MyTable.Column TableElement Msg
sizeColumn = 
    MyTable.veryCustomColumn
        { name = "Size"
        , viewData = viewSize << .size
        , sorter = MyTable.increasingOrDecreasingBy .size
        }

viewSize: Float -> MyTable.HtmlDetails Msg
viewSize b = 
    let 
        kb = b/1024
        mb = kb/1024
        gb = mb/1024
        str = 
        if round(gb) > 1 then
            slice 0 4 (toString gb ) ++ " GB"
        else 
            if round(mb) > 1 then
                slice 0 4 (toString mb ) ++ " MB"
                else
                    if round(kb) > 1 then
                        slice 0 4 (toString kb ) ++ " KB"
                    else
                        if b == 0 then
                            ""
                        else 
                            toString b ++ " B"
    in
        MyTable.HtmlDetails [] [span [style [("overflow","hidden")]] [text str] ]
        

toRowAttrs : TableElement -> List (Attribute Msg)
toRowAttrs tableElement =
  [ style   [ ("background", 
                if ( tableElement.selected ) then 
                     "#ff9900" 
                else "rgba(0, 0, 0, 0)")
            ]
  ]

checkboxColumn : MyTable.Column TableElement Msg
checkboxColumn =
  MyTable.veryCustomColumn
    { name = "selectAllCheckbox"
    , viewData = viewCheckbox
    , sorter = MyTable.unsortable    
    }


viewCheckbox : TableElement -> MyTable.HtmlDetails Msg
viewCheckbox  tableElements =
  MyTable.HtmlDetails []
    [ input [ type_ "checkbox"
            , checked tableElements.selected
            , onClick (ToggleSelected (tableElements.prefix ++ tableElements.keyName) )
            ] []
    ]

--------------------------------------------------------------------------------

customThead model acceptableElements headers =
    HtmlDetails [] (List.map (customTheadHelp model acceptableElements) headers)

--------------------------------------------------------------------------------

customTheadHelp model acceptableElements (name, status, onClick) =
    case name of 
        "selectAllCheckbox" -> 
            let 
                content = 
                [ input [ type_ "checkbox"
                        , checked (selectAllStatus model acceptableElements)
                        , Html.Events.onClick (ToggleSelectAll (model, acceptableElements) )
                        , style[("float","left")]
                        ] 
                        []
                ]
            in
                Html.th [ onClick ] content
        _ ->
            let
                content = sortControl name status
            in
                Html.th [ onClick ] content
        
--------------------------------------------------------------------------------

sortControl name status = 
    case status of
        Unsortable ->
            [ Html.text name ]
        Sortable selected ->
            [ Html.text name, if selected then darkGreyDown else lightGreyDown ]
        Reversible Nothing ->
            [ Html.text name, lightGreyUpDown ]
        Reversible (Just isReversed) ->
            [ Html.text name, if isReversed then darkGreyUp else darkGreyDown ]
        
--------------------------------------------------------------------------------


selectAllStatus: Model -> List TableElement -> Bool
selectAllStatus model acceptableElements =
    if (Set.size model.selectedKeys) == (List.length acceptableElements) 
    && Set.size model.selectedKeys > 0 then
        True
    else
        False
        
--------------------------------------------------------------------------------

darkGreyDown : Html msg
darkGreyDown =
  Html.span [ Attr.class "icon ion-arrow-down-c", Attr.style [("position","relative"), ("margin-left","5px")] ] []

darkGreyUp : Html msg
darkGreyUp =
  Html.span [ Attr.class "icon ion-arrow-up-c", Attr.style [("position","relative"), ("margin-left","5px")] ] []

lightGreyUpDown : Html msg
lightGreyUpDown =
  Html.span [ Attr.class "icon ion-arrow-graph-up-left ", Attr.style [("position","relative"), ("margin-left","5px")] ] []

lightGreyDown : Html msg
lightGreyDown  =
  Html.span [ Attr.class "icon ion-ios-arrow-thin-down", Attr.style [("position","relative"), ("margin-left","5px")] ] []

lightGreyUp : Html msg
lightGreyUp  =
  Html.span [ Attr.class "icon ion-ios-arrow-thin-up", Attr.style [("position","relative"), ("margin-left","5px")]] []

    
