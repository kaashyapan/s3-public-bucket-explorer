module Update exposing (..)

import MyTable exposing (defaultCustomizations)
import Model exposing (..)
import Dict exposing (remove, member)
import List exposing (map)
import Set exposing (filter, size, empty, fromList)
import Helpers exposing (..)
import Ports exposing (..)

--UPDATE cache

{--
updateWithCache : Msg -> Model -> ( Model, Cmd Msg )
updateWithCache msg model = 
    let
        ( newModel, cmds ) = update msg model
        cacheModel = setState newModel


    in
        ( newModel
        , Cmd.batch [ setStorage cacheModel, cmds ]
        )
--}

-- UPDATE

update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        UrlChange v ->
            if (model.ignoreUrlChange == False) then
                if (String.isEmpty v.hash) then
                    ( { model | pathPrefix = "", loc = v }, Cmd.none)
                else
                    let 
                        pathPrefix = String.dropLeft 1 v.hash 
                    in
                        ( { model | pathPrefix = pathPrefix, loc = v }, openDir pathPrefix)
            else
                ( { model | ignoreUrlChange = False, loc = v } , Cmd.none)
            
        FetchBucket ->
            let 
                str = validateBucketName model.pathPrefix
            in
                case str of 
                    Err v -> 
                        ( { model | logs = model.logs ++ [("ERROR",[v])] }
                        , Cmd.none)

                    Ok pathPrefix ->
                        let 
                            cleanBkt = remove pathPrefix model.bkt
                        in
                            getListObject {model | bkt = cleanBkt }
                
        ErrLog v ->
            ( { model | logs = model.logs ++ [("ERROR", v)]} , Cmd.none)
            
        InfoLog v ->
            ( { model | logs = model.logs ++ [("INFO", v)]} , Cmd.none)

        SuccessLog v ->
            ( { model | logs = model.logs ++ [("SUCCESS", v)]} , Cmd.none)
            
        NewPath v ->
            ( {model | pathPrefix = v} , Cmd.none )
        
        NewObject str ->
            let 
                ( newModel, dir) = updateBucket str model 
            in
                case dir of 
                    Ok dir ->
                        (newModel , displayTable dir.prefix)
                    Err err ->
                        (newModel, Cmd.none)                
                        
        DisplayTable dirName -> 
            updateTable dirName model 
            
        
        ToggleSelected v ->
            ( { model | selectedKeys = toggle v model.selectedKeys }, Cmd.none )  
        
        SetTableState newState ->
            ( { model | tableState = newState }, Cmd.none )

        SetQuery newQuery ->
            ( { model | query = newQuery }  , Cmd.none  )
            
        Download ->
            let 
                selectedDirs = 
                        Set.filter (\ key -> if String.endsWith "/" key then
                                                True
                                             else
                                                False
                                    ) model.selectedKeys

                (selectedKeys, logs, cmd) =                 
                    if ( Set.size selectedDirs ) > 0 then
                        let 
                            v = "You cannot download a directory. You may open it and select all elements"
                        in
                            (model.selectedKeys,  model.logs ++ [("ERROR",[v])] , Cmd.none)
                    else
                        (Set.empty, model.logs, downloadElement model)
                        
            in
                ( { model | selectedKeys = selectedKeys, logs = logs }, cmd)
                    
        DownloadTorrent ->
            ( { model | selectedKeys = Set.empty }, downloadTorrent model)
    
        OpenDir pathPrefix ->
            if member pathPrefix model.bkt then
                ({model | pathPrefix = pathPrefix
                        , selectall = False
                        , selectedKeys = Set.empty }, displayTable pathPrefix )
            else
                ({model | pathPrefix = pathPrefix
                        , selectall = False
                        , selectedKeys = Set.empty }, fetchDir)

        FetchMore v ->
            getMoreListObject {model | pathPrefix = v}
            
        ToggleSelectAll (model, acceptableElements) ->
            let 
                selectall = not model.selectall
                selectedKeys = 
                    if selectall then
                        let 
                            list = List.map (\ v -> v.prefix ++ v.keyName) acceptableElements
                        in 
                            Set.fromList list
                    else
                        Set.empty
            in  
                ( { model | selectedKeys = selectedKeys , selectall = selectall}, Cmd.none )
                
        BktLoc v -> 
            bktLocation v model

        ReadCache cacheModel ->
            fromState cacheModel model.loc
            
        SetCache flag ->
            let 
                cacheModel = setState model
            in
                (model, setStorage cacheModel)

        EmptyMsgs ->
            ({model | logs = [] }, Cmd.none)