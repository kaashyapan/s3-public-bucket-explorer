import Navigation exposing (..)
import Model exposing (..)
import View exposing (..)
import Update exposing (..)
import Ports exposing (..)

-- MAIN

main : Program Never Model Msg
main  =
    Navigation.program UrlChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }            
        
-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [    errLog ErrLog  
            ,infoLog InfoLog
            ,successLog SuccessLog
            ,listObjectv2Resp NewObject
            ,bucketLocation BktLoc
            ,cacheData ReadCache
            ,rqstCacheupdate SetCache
        ]
