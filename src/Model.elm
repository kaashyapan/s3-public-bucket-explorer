module Model exposing (..)
import MyTable exposing (..)
import Dict exposing (..)
import List exposing (..)
import Set exposing (..)
import Navigation exposing (..)
import Extras exposing (..)

-- MODEL

type alias Bkt = Dict String Dir

type alias Dir =
  { name : String
  , prefix : String
  , isTruncated : Bool
  , commonPrefix : List String
  , contents: List BktElement
  , continuationToken : Maybe String
  }

type alias BktElement = 
  { keyName : String
  , lastModified : String
  , eTag : String
  , size : Float
  , storageClass : String
  }

type alias TableElement = 
  { bucketName : String
  , prefix : String
  , keyName : String
  , lastModified : String
  , eTag : String
  , size : Float
  , storageClass : String
  , selected : Bool
  }

type alias Model =
  { pathPrefix : String
  , bkt : Bkt
  , bktLocation : Dict String String
  , query : String
  , selectall : Bool
  , selectedKeys : Set String
  , tableState : MyTable.State
  , ignoreUrlChange : Bool
  , loc : Location
  , logs : List (String, List String)
  }


initialModel: Location -> Model
initialModel location =
  { pathPrefix = ""
  , bkt = Dict.empty
  , bktLocation = Dict.empty
  , query = ""
  , selectall = False
  , selectedKeys = Set.empty
  , tableState = MyTable.initialSort ""
  , ignoreUrlChange = False
  , loc = location
  , logs = []
  }
  
type alias Loc = { bucket : String, region : String }  

tableElementInit: TableElement
tableElementInit = TableElement "Bktname" "prefix" "keyName" "lastMod" "etag" 0 "storage" False 

init :  Navigation.Location -> ( Model, Cmd Msg )
init  location = 
    (initialModel location, Extras.message ( UrlChange location))

emptyDir = 
     Dir "" "" False [""] [(BktElement "" "" "" 0 "")] Nothing


--------------------------------------------------------------------------------
-- CACHE

type alias CacheModel =
  { pathPrefix : String
  , bkt : List (String, Dir)
  , bktLocation : List (String, String)
  , query : String
  , selectall : Bool
  , selectedKeys : List String
  , ignoreUrlChange : Bool
  , logs : List (String, List String)
  , hash : String
  }

---------------------------------------------------------------------------------
-- MESSAGES

type Msg =  FetchBucket
            | NewPath String
            | NewObject String
            | ToggleSelected String
            | SetTableState MyTable.State
            | UrlChange Navigation.Location
            | ErrLog (List String)
            | InfoLog (List String)
            | SuccessLog (List String)
            | DisplayTable String
            | SetQuery String
            | Download 
            | DownloadTorrent
            | OpenDir String
            | FetchMore String
            | ToggleSelectAll (Model, List TableElement)
            | BktLoc String
            | ReadCache CacheModel
            | SetCache Bool
            | EmptyMsgs

