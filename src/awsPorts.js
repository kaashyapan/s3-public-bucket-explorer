var S3 = new AWS.S3({
    apiVersion: '2006-03-01'
});

function s3ListUnauthenticatedRqst(args) {
    var obj = JSON.parse(args);
    //  obj.MaxKeys = 50;
    var info = [];
    info.push("..Sending request to fetch " + obj.Bucket + "/" + obj.Prefix);
    app.ports.infoLog.send(info);
    var callAWS = S3.makeUnauthenticatedRequest('listObjectsV2', obj).promise();

    callAWS.then(function(s3ListJson) {
        if (s3ListJson.Contents.length == 0) {
            var errors = [];
            errors.push(" Error in retreiving listObjectsV2 unauthenticated; ");
            errors.push("Bucket name: " + obj.Bucket + "; Prefix: " + obj.Prefix + " " + "; " +
                "Check prefix and delimiter. Bucket may exist but prefix may not.");
            app.ports.errLog.send(errors);

        }
        else {
            var x = JSON.stringify(s3ListJson);
            var successLog = [];
            successLog.push("Found " + obj.Bucket + "/" + obj.Prefix);
            app.ports.successLog.send(successLog);
            app.ports.listObjectv2Resp.send(x);
            sendRqstCacheupdate();
        }
    }).catch(function(err) {
        var errors = [];
        errors.push(" Error in retreiving listObjectsV2 unauthenticated; ");
        errors.push("Bucket name: " + obj.Bucket + "; Prefix: " + obj.Prefix + " " + err.message);
        app.ports.errLog.send(errors);
    });
}

function s3getObjectUnauth(args) {
    var obj = JSON.parse(args);
    var info = [];
    info.push("...Sending request to download " + obj.Bucket + "/" + obj.Key);
    app.ports.infoLog.send(info);

    var callAWS = S3.makeUnauthenticatedRequest('getObject', obj).promise();

    callAWS.then(function(resp) {
        var blob = new Blob([resp.Body], {
            type: resp.ContentType
        });
        saveAs(blob, obj.Key);
        var successLog = [];
        successLog.push("Downloading " + obj.Key);
        app.ports.successLog.send(successLog);
    }).catch(function(err) {
        var errors = [];
        errors.push(" Error in getObject; ");
        errors.push(err.message);
        app.ports.errLog.send(errors);
    });
}

function s3downObjectUnauth(args) {
    var obj = JSON.parse(args);
    var info = [];
    info.push("...Sending request to download " + encodeURI(obj.uri));
    app.ports.infoLog.send(info);
    //    window.location = encodeURI(obj.uri);
    downloadFile(encodeURI(obj.uri));
}

function s3getObjectTorrentUnauth(args) {
    var obj = JSON.parse(args);
    var info = [];
    info.push("...Sending request to download torrent for " + obj.Bucket + "/" + obj.Key);
    app.ports.infoLog.send(info);
    var callAWS = S3.makeUnauthenticatedRequest('getObjectTorrent', obj).promise();

    callAWS.then(function(resp) {
        var blob = new Blob([resp.Body], {
            type: resp.ContentType
        });
        saveAs(blob, obj.Key + ".torrent");
        var successLog = [];
        successLog.push("Downloading torrent for " + obj.Key);
        app.ports.successLog.send(successLog);
    }).catch(function(err) {
        var errors = [];
        errors.push(" Error in getObjectTorrent; ");
        errors.push(err.message);
        app.ports.errLog.send(errors);
    });
}

function s3getBucketLocation(args) {
    var obj = JSON.parse(args);
    var info = [];
    info.push("...Getting bucket location " + obj.Bucket);
    app.ports.infoLog.send(info);
    var callAWS = S3.makeUnauthenticatedRequest('getBucketLocation', obj).promise();

    callAWS.then(function(s3Json) {
        s3Json.Bucket = obj.Bucket;
        var x = JSON.stringify(s3Json);
        app.ports.bucketLocation.send(x);
        var successLog = [];
        successLog.push("Found " + obj.Bucket + " in " + s3Json.LocationConstraint);
        app.ports.successLog.send(successLog);
    }).catch(function(err) {
        var errors = [];
        errors.push(" Error in getting bucket location; ");
        errors.push("Bucket name: " + obj.Bucket + "; " + err.message);
        app.ports.errLog.send(errors);
        errors = [];
        errors.push(" Check the bucket ACL and allow access to GetBucketLocation action");
        app.ports.errLog.send(errors);
    });
}


function initMsgs() {

    var info = [];
    info.push(" Source can be found in https://bitbucket.org/sundernswamy/s3-public-bucket-explorer ");
    app.ports.infoLog.send(info);
    info = [];
    info.push(" This app is hosted on https://sundernswamy.bitbucket.io/S3PublicExplorer/ ");
    app.ports.infoLog.send(info);

    var storedState = localStorage.getItem('elm-s3publicexplorer');
    var startingState = storedState ? JSON.parse(storedState) : null;
    if (startingState !== null) {
        app.ports.cacheData.send(startingState)
    }
}

function sendRqstCacheupdate() {
    app.ports.rqstCacheupdate.send(true);
}
