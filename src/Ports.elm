port module Ports exposing (..)

import Model exposing (..)

port toListObjectsv2 : String -> Cmd msg
port toGetObject : String -> Cmd msg
port toGetObjectTorrent : String -> Cmd msg
port toGetBucketLocation : String -> Cmd msg
port toDownObject : String -> Cmd msg
port setStorage : CacheModel -> Cmd msg

port errLog : (List String -> msg) -> Sub msg
port successLog : (List String -> msg) -> Sub msg
port infoLog : (List String -> msg) -> Sub msg
port listObjectv2Resp : (String -> msg) -> Sub msg 
port bucketLocation : (String -> msg) -> Sub msg
port cacheData : (CacheModel -> msg) -> Sub msg
port rqstCacheupdate : (Bool -> msg) -> Sub msg