### AWS S3 bucket explorer for public buckets  ###

This is written using ELM 0.18 with plain javascript that uses the AWS SDK. 

### Browser compatibilty ###
Tested with Chrome 56, Opera 43 & Firefox 51.   
Does not work with Safari & IE

### Dependencies ###

Refer to elm-package.json for ELM package dependencies.

Other javascript libs  

* [http://ionicons.com/](Link URL)  - for icons   
* [https://github.com/eligrey/FileSaver.js/](Link URL) - for file downloads 
* [https://picturepan2.github.io/spectre/](Link URL) - for CSS

### Restrictions ### 

* Works only with buckets that use slash '/' as the delimiter.
* The bucket must have appropriate policy ACL. See AWS-stuff/sample_ACL.json
* The bucket must have CORS enabled. See AWS-stuff/sample-CORS.xml
* The download function works on Chrome. If you want force certain browsers to download the file instead of opening it. You have to set the metadata "content-disposition" to "attachment" while uploading the file to S3.

### Usage ###
This app is hosted on [https://sundernswamy.bitbucket.io/S3PublicExplorer/](Link URL)  

Alternatively you may choose to copy the contents of hosted app to you bucket and configure a static website